import { Box } from '@mui/material'
import type { ReactNode } from 'react'

interface FlexProps {
  children: ReactNode
  column?: boolean
}

export default function FlexContainer({ children, column }: FlexProps) {
  const align = { [column ? 'alignItems' : 'justifyContent']: 'center' }
  return (
    <Box
      display="flex"
      flexDirection={column ? 'column' : 'row'}
      width="100%"
      my={2}
      {...align}
    >
      {children}
    </Box>
  )
}
