import { Container, styled } from '@mui/material'

const StyledContainer = styled(Container)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  [theme.breakpoints.up('lg')]: {
    maxWidth: 900,
  },
  [theme.breakpoints.down('lg')]: {
    maxWidth: 600,
  },
}))

export default StyledContainer
