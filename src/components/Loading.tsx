import { Typography, Box } from '@mui/material'

export default function Loading({ show }: { show: boolean }) {
  return (
    <Box display="flex" alignItems="center" justifyContent="center">
      <Typography fontSize={24}>{show ? '⬇' : null}</Typography>
    </Box>
  )
}
