import { Typography } from '@mui/material'

export default function Title(props: { text: string }) {
  return (
    <Typography variant="h4" my={2} color="primary">
      {props.text}
    </Typography>
  )
}
