import { Paper } from '@mui/material'
import {
  VictoryChart,
  VictoryLine,
  VictoryZoomContainer,
  VictorySelectionContainer,
  VictorySelectionContainerProps,
  VictoryLineProps,
  VictoryChartProps,
} from 'victory'
import type { Point, Domain } from '../types'
import { theme } from '../providers/StyleProvider'

const CHART_STYLE: VictoryChartProps['style'] = {
  parent: {
    marginLeft: 32,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}

const LINE_STYLE: VictoryLineProps['style'] = {
  data: { stroke: theme.palette.secondary.main },
}

interface ChartProps {
  data: Point[]
  zoomDomain: Domain
  dataDomain?: Domain
  onChangeZoomDomain: (domain: Domain) => void
  yAxisSize: number
  selectionMode: boolean
  setSelectionMode: (value: boolean) => void
}

export default function Chart({
  data,
  zoomDomain,
  dataDomain,
  onChangeZoomDomain,
  yAxisSize,
  selectionMode,
  setSelectionMode,
}: ChartProps) {
  if (!dataDomain) {
    return null
  }

  function handleOnSelection(_: unknown, { x: selectedDomain }: { x: Domain }) {
    onChangeZoomDomain(selectedDomain)
    setTimeout(() => setSelectionMode(false), 0)
  }

  const container = selectionMode ? (
    <VictorySelectionContainer
      selectionDimension="x"
      onSelection={
        handleOnSelection as unknown as VictorySelectionContainerProps['onSelection']
      }
    />
  ) : (
    <VictoryZoomContainer
      zoomDimension="x"
      onZoomDomainChange={({ x }) => onChangeZoomDomain(x as Domain)}
      zoomDomain={{ x: zoomDomain }}
    />
  )

  return (
    <Paper sx={{ height: 400, width: '100%' }}>
      <VictoryChart
        containerComponent={container}
        style={CHART_STYLE}
        domain={{
          x: selectionMode ? zoomDomain : dataDomain,
          y: [-yAxisSize, yAxisSize],
        }}
      >
        <VictoryLine data={data} style={LINE_STYLE} />
      </VictoryChart>
    </Paper>
  )
}
