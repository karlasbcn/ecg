import { Slider as MUISlider, Typography, Box } from '@mui/material'

interface SliderProps {
  onChange: (value: number) => void
  label: string
  step: number
  value: number
  min: number
  max: number
}

export default function Slider(props: SliderProps) {
  const { onChange, label, ...rest } = props
  function handleOnChange(_: unknown, value: number | number[]) {
    onChange(value as number)
  }

  return (
    <Box width="100%">
      <Typography>{label}</Typography>
      <MUISlider valueLabelDisplay="auto" onChange={handleOnChange} {...rest} />
    </Box>
  )
}
