import { Switch as MUISwitch, Typography, Box } from '@mui/material'

interface SwitchProps {
  checked: boolean
  onChange: () => void
  label: string
}

export default function Switch(props: SwitchProps) {
  const { label, ...rest } = props
  return (
    <Box display="flex" alignItems="center" width="100%">
      <Typography>{label}</Typography>
      <MUISwitch {...rest} />
    </Box>
  )
}
