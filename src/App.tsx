import { useState } from 'react'
import { useData, useFilter } from './hooks'
import {
  Chart,
  PageContainer,
  Title,
  Slider,
  Switch,
  FlexContainer,
  Loading,
} from './components'

export default function Index() {
  const { zoomDomain, setZoomDomain, yAxisSize, setYAxisSize } = useFilter()
  const { data, dataDomain, fetching } = useData(zoomDomain)
  const [selectionMode, setSelectionMode] = useState(false)

  return (
    <PageContainer>
      <Title text="Idoven.ai Coding Challenge" />
      <FlexContainer column>
        <Slider
          step={1500}
          value={yAxisSize}
          onChange={setYAxisSize}
          min={500}
          max={35000}
          label="Vertical axis size"
        />
        <FlexContainer>
          <Switch
            checked={selectionMode}
            onChange={() => setSelectionMode(!selectionMode)}
            label="Selection Mode"
          />
          <Loading show={fetching} />
        </FlexContainer>
      </FlexContainer>
      <Chart
        data={data}
        onChangeZoomDomain={setZoomDomain}
        zoomDomain={zoomDomain}
        dataDomain={dataDomain}
        yAxisSize={yAxisSize}
        selectionMode={selectionMode}
        setSelectionMode={setSelectionMode}
      />
    </PageContainer>
  )
}
