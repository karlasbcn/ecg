import { useQueryParam, StringParam, NumberParam } from 'use-query-params'
import type { Domain } from '../types'

export const DEFAULT_ZOOM_DOMAIN: Domain = [0, 500]
export const DEFAULT_Y_AXIS_SIZE = 10000

export default function useFilter() {
  const [zoomDomainParam, setZoomDomainParam] = useQueryParam(
    'zoomDomain',
    StringParam
  )
  const [yAxisSize, setYAxisSize] = useQueryParam('timeAxis', NumberParam)

  const zoomDomain = zoomDomainParam
    ? (zoomDomainParam!.split(',').map((n) => Number(n)) as Domain)
    : DEFAULT_ZOOM_DOMAIN

  function setZoomDomain(domain: Domain) {
    setZoomDomainParam(domain.join(','))
  }

  return {
    setZoomDomain,
    zoomDomain,
    setYAxisSize,
    yAxisSize: yAxisSize ? Number(yAxisSize) : DEFAULT_Y_AXIS_SIZE,
  }
}
