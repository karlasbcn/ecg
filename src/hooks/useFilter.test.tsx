import { renderHook, act } from '@testing-library/react-hooks'
import { useQueryParam } from 'use-query-params'
import useFilter, {
  DEFAULT_ZOOM_DOMAIN,
  DEFAULT_Y_AXIS_SIZE,
} from './useFilter'

jest.mock('use-query-params')

const mockUseQueryParam = useQueryParam as jest.MockedFunction<
  typeof useQueryParam
>

describe('useFilter', () => {
  it('returns zoom domain from URL', () => {
    mockUseQueryParam.mockReturnValueOnce(['1,2', () => null])
    mockUseQueryParam.mockReturnValueOnce(['200', () => null])
    const { result } = renderHook(useFilter)
    const { zoomDomain, yAxisSize } = result.current
    expect(zoomDomain).toEqual([1, 2])
    expect(yAxisSize).toEqual(200)
  })
  it('returns default params values if query params not present', () => {
    mockUseQueryParam.mockReturnValue([undefined, () => null])
    const { result } = renderHook(useFilter)
    const { zoomDomain, yAxisSize } = result.current
    expect(zoomDomain).toEqual(DEFAULT_ZOOM_DOMAIN)
    expect(yAxisSize).toEqual(DEFAULT_Y_AXIS_SIZE)
  })
  it('correctly sets zoom domain', () => {
    const mockSetZoomDomainParam = jest.fn()
    mockUseQueryParam.mockReturnValue([undefined, mockSetZoomDomainParam])
    const { result } = renderHook(useFilter)
    act(() => result.current.setZoomDomain([3, 4]))
    expect(mockSetZoomDomainParam).toHaveBeenCalledWith('3,4')
  })
})
