import { useCallback, useEffect, useState } from 'react'
import type { Point, Domain } from '../types'

type DataChunks = (Point[] | null)[]

const NUM_CHUNK_FILES = 4548
const DOMAIN_SPAN_PER_CHUNK = 40
export const MAX_POINTS = 500

export async function fetchSingleChunk(chunk: number) {
  try {
    const raw = await fetch(`data/data${chunk.toString().padStart(4, '0')}`)
    const text = await raw.text()
    return text
      .split('\n')
      .filter((line) => line.length && !line.includes('Time'))
      .map((line) => {
        const [x, y] = line.split(',').map((n) => Number(n))
        return { x, y } as Point
      })
  } catch (error) {
    const { message } = error as { message: string }
    alert(`Error: "${message}"`)
    throw message
  }
}

export async function fetchChunks(
  chunksIndexes: number[],
  dataChunks: DataChunks
) {
  const chunks = await Promise.all(chunksIndexes.map(fetchSingleChunk))
  const updatedDataChunks = [...dataChunks]
  chunksIndexes.forEach((chunkIndex, i) => {
    updatedDataChunks[chunkIndex] = chunks[i]
  })
  const normalized = normalizeDataChunks(updatedDataChunks)
  const dataDomain: Domain = [normalized.at(0)!.x, normalized.at(-1)!.x]
  return {
    dataChunks: updatedDataChunks,
    dataDomain,
  }
}

export function normalizeDataChunks(dataChunks: DataChunks) {
  return dataChunks.filter((chunk) => chunk !== null).flat() as Point[]
}

export function filterData(data: Point[], [start, end]: Domain) {
  const filtered = data.filter(({ x }) => x >= start && x <= end)
  const factor = Math.ceil(filtered.length / MAX_POINTS)
  if (factor <= 1) {
    return filtered
  }
  return filtered.filter((_, i) => i % factor === 0)
}

interface State {
  dataChunks: DataChunks
  fetching: boolean
  dataDomain?: Domain
}

export default function useData(zoomDomain: Domain) {
  const [state, setFullState] = useState<State>({
    dataChunks: Array(NUM_CHUNK_FILES).fill(null),
    fetching: false,
  })

  const { dataChunks, fetching, dataDomain } = state
  const [zoomDomainStart, zoomDomainEnd] = zoomDomain

  const setState = useCallback(
    (newState: Partial<State>) => setFullState({ ...state, ...newState }),
    [state]
  )

  useEffect(() => {
    async function checkMissingChunks() {
      const start = Math.max(
        0,
        Math.floor(zoomDomainStart / DOMAIN_SPAN_PER_CHUNK) - 5
      )
      const end = Math.min(
        NUM_CHUNK_FILES,
        Math.ceil(zoomDomainEnd / DOMAIN_SPAN_PER_CHUNK) + 5
      )
      const missingChunks = Array(end - start)
        .fill(0)
        .map((_, i) => {
          return i + start
        })
        .filter((n) => dataChunks[n] === null)

      if (!fetching && missingChunks.length) {
        setState({ fetching: true })
        const newState = await fetchChunks(missingChunks, dataChunks)
        setState({ ...newState, fetching: false })
      }
    }
    checkMissingChunks()
  }, [zoomDomainStart, zoomDomainEnd, dataChunks, fetching, setState])

  return {
    data: filterData(normalizeDataChunks(dataChunks), zoomDomain),
    dataDomain,
    fetching,
  }
}
