import { renderHook } from '@testing-library/react-hooks'
import { Point, Domain } from '../types'
import useData, {
  fetchSingleChunk,
  fetchChunks,
  normalizeDataChunks,
  filterData,
  MAX_POINTS,
} from './useData'

const mockFetchText = jest.fn()
global.fetch = jest.fn(() =>
  Promise.resolve({
    text: mockFetchText,
  })
) as jest.Mock

describe('useData', () => {
  it('fetches chunks automatically depending on zoom domain', async () => {
    mockFetchText.mockResolvedValue(`1,2,3\n3,4,5`)
    const { result, waitForNextUpdate } = renderHook(() => useData([0, 500]))
    expect(result.current.data.length).toEqual(0)
    await waitForNextUpdate()
    expect(result.current.data.length).toBeGreaterThan(0)
  })
  it('ignores data lines including "Time" and blank lines', async () => {
    mockFetchText.mockResolvedValue(`1,2,3\nTime,4,5\n\n`)
    const points = await fetchSingleChunk(0)
    expect(points.length).toEqual(1)
  })
  it('normalizes datachunks', () => {
    const data = normalizeDataChunks([
      null,
      [
        { x: 1, y: 2 },
        { x: 3, y: 4 },
      ],
      null,
      [{ x: 5, y: 6 }],
    ])
    expect(data).toEqual([
      { x: 1, y: 2 },
      { x: 3, y: 4 },
      { x: 5, y: 6 },
    ])
  })
  it('fetches missing chunks correctly', async () => {
    mockFetchText.mockResolvedValue(`1,2\n3,4`)
    const { dataChunks } = await fetchChunks([1, 3], Array(4).fill(null))
    const mockChunk = [
      { x: 1, y: 2 },
      { x: 3, y: 4 },
    ]
    expect(dataChunks[0]).toEqual(null)
    expect(dataChunks[1]).toEqual(mockChunk)
    expect(dataChunks[2]).toEqual(null)
    expect(dataChunks[3]).toEqual(mockChunk)
  })
  it('filters data depending on zoom domain', () => {
    const mockData: Point[] = Array(50000)
      .fill(0)
      .map((_, i) => ({ x: i, y: i }))
    expect(filterData(mockData, [0, 9]).length).toEqual(10)
    expect(filterData(mockData, [0, 99999999999999999]).length).toEqual(
      MAX_POINTS
    )
    const mockDomain: Domain = [0, 100]
    const result = filterData(mockData, mockDomain)
    const ok = result.every(({ x }) => x >= mockDomain[0] && x <= mockDomain[1])
    expect(ok).toBe(true)
  })
})
