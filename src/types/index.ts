export type Point = { x: number; y: number }
export type Domain = [number, number]
