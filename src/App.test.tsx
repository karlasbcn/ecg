import { render, screen } from '@testing-library/react'
import App from './App'
import { useData, useFilter } from './hooks'

jest.mock('./hooks/')
const mockUseData = useData as jest.MockedFunction<typeof useData>
const mockUseFilter = useFilter as jest.MockedFunction<typeof useFilter>

describe('App', () => {
  it('renders mandatory header', () => {
    mockUseData.mockReturnValue({
      data: [],
      dataDomain: undefined,
      fetching: true,
    })

    mockUseFilter.mockReturnValue({
      setZoomDomain: () => null,
      zoomDomain: [1, 2],
      setYAxisSize: () => {},
      yAxisSize: 1000,
    })

    render(<App />)
    const header = screen.getByText(/Idoven.ai Coding Challenge/)
    expect(header).toBeInTheDocument()
    expect(mockUseData).toHaveBeenCalled()
  })
})
