import { ThemeProvider, createTheme } from '@mui/material/styles'
import CssBaseline from '@mui/material/CssBaseline'
import type { ReactNode } from 'react'

export const theme = createTheme({
  palette: {
    primary: {
      main: '#002735',
    },
    secondary: {
      main: '#39c4d8',
    },
    background: {
      default: '#f2f2f2',
    },
  },
})

export default function StyleProvier({ children }: { children: ReactNode }) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  )
}
