![](https://gcdnb.pbrd.co/images/SBmh9nswYEgg.png?o=1)

## Installation

After cloning this repo, run `yarn install` or `npm install` to install the third party libraries. After this, the `7z` file will be automatically uncompressed and splitted into several small chunk files inside the `/public` folder.

## How it works

An array with N positions (where N is the number of chunk data files) will be initialized with null values. Chunk files will be fetched according to the position in the chart, filling the array. If user moves across the chart or zooms out, new chunks will be downloaded (similar to a virtual table with paginated data).

## Chart

[Victory](https://formidable.com/open-source/victory/) library is used to display the chart. It allows you to zoom and select areas.

## URL Query Params

Position and zoom on chart are persisted in the URL, allowing to share an specific position of the chart.

## Controls

- We can modify the dimension of the Y-axis through an slider.
- Scrolling horizontally the chart will allow us to move it horizontally. Scrolling vertically will zoom in/out the chart.
- There's a switch which allows to change the chart scroll into a selection area, which will zoom into the selected region. _(Known issue: after the first selection, next selections will start in a position not according to the mouse position)_.
